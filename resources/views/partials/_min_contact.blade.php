<section class="newsletter-section">
    <div class="container">
        <div class="newsletter-sec response_container">
            <div class="row align-items-center">
                <div class="col-lg-4">
                    <div class="newsz-ltr-text">
                        <h2>Contacto</h2>
                        <button class="btn-default" onclick="ajaxContact()">Send <i class="fa fa-long-arrow-alt-right"></i></button>
                    </div><!--newsz-ltr-text end-->
                </div>
                <div class="col-lg-8">
                    <form class="newsletter-form">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input type="text" id="name" name="name" placeholder="Nombre">
                                </div><!--form-group end-->
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="email" id="email" name="email" placeholder="Email">
                                </div><!--form-group end-->
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea id="message" name="message" placeholder="Mensaje"></textarea>
                                </div><!--form-group end-->
                            </div>
                        </div>
                    </form><!--newsletter-form end-->
                </div>
            </div>
        </div><!--newsletter-sec end-->
    </div>
</section><!--newsletter-sec end-->
